from cv2 import cv2
import numpy as np

cap = cv2.VideoCapture(0)

def extract_threshold(self, method=1, min_thresh=0, choice=0, min_val=150, max_val=200, thickness=1):

    thresh = 0

    assert method < 2, 'Method should be either 0 or 1 for Thresholding, Refer docstring'

    if method == 0:
        edged = self.extract_edges(choice=choice, min_val=min_val, max_val=max_val, thickness=thickness)
        _, contours, _ = cv2.findContours(edged, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
        mask = np.zeros(self.image.shape[:2], dtype=self.image.dtype)
        thresh = cv2.drawContours(mask, contours, -1, 255, -1)

    elif method == 1:

        assert min_thresh >= 0, 'min_val parameter must be greater than or equal to 0'

        blur = cv2.GaussianBlur(self.image, (5, 5), 0)
        blur__blue, blur__green, blur__red = cv2.split(blur)
        ret1, thresh__blue = cv2.threshold(blur__blue, min_thresh, 255, cv2.THRESH_BINARY + cv2.THRESH_OTSU)
        ret2, thresh__green = cv2.threshold(blur__green, min_thresh, 255, cv2.THRESH_BINARY + cv2.THRESH_OTSU)
        ret3, thresh__red = cv2.threshold(blur__red, min_thresh, 255, cv2.THRESH_BINARY + cv2.THRESH_OTSU)
        thresh__b_g = cv2.bitwise_or(thresh__blue, thresh__green)
        thresh = cv2.bitwise_or(thresh__b_g, thresh__red)
        morphological_kernel = np.ones((3, 3), np.uint8)
        eroded_frame = cv2.erode(thresh, morphological_kernel, iterations=2)
        dilated_frame = cv2.dilate(eroded_frame, morphological_kernel, iterations=2)
        thresh = dilated_frame

    return thresh

def extract_edges(self, choice=3, min_val=100, max_val=200, thickness=1):

    assert thickness > 0, 'Thickness parameter must be greater than 0'
    assert min_val > 0, 'min_val parameter must be greater than 0'
    assert max_val > 0, 'max_val parameter must be greater than 0'
    assert 0 <= choice < 4, 'Choice parameter must be 0, 1, 2 or 3'

    foreground = self.extract_foreground()[0]
    blur = 0

    if choice == 0:
        blur = cv2.medianBlur(foreground, 3)
    elif choice == 1:
        blur = cv2.bilateralFilter(foreground, 9, 75, 75)
    elif choice == 2:
        blur = cv2.GaussianBlur(foreground, (3, 3), 0)
    elif choice == 3:
        blur = self.image

    kernel = cv2.getStructuringElement(cv2.MORPH_ELLIPSE, (3, 3))
    edged = cv2.Canny(blur, min_val, max_val)
    edged = cv2.morphologyEx(edged, cv2.MORPH_DILATE, kernel, iterations=thickness)
    return edged

def create_object_mask(self, contour):

    mask = np.zeros(self.image.shape[:2], dtype=self.image.dtype)
    mask = cv2.drawContours(mask, [contour], -1, 255, -1)
    return mask

def extract_plane(self, method=0, edged=None, foreground=None, return_pill=True, thresholded=None,
                           min_height=10,
                           min_width=10,
                           crop=224):

    assert -1 < method < 2, 'Method must be 0 or 1, Refer Docstring'
    assert edged is not None, 'Edged frame cannot be None'
    assert foreground is not None, 'foreground frame cannot be none'

    rect_list = []

    if method == 0:

        im2_canny, contours_canny, hierarchy_canny = cv2.findContours(edged, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
        cv2.waitKey(0)

        for component in zip(contours_canny, hierarchy_canny[0]):

            current_contour = component[0]
            current_hierarchy = component[1]

            if current_hierarchy[3] != -1:

                rect = cv2.boundingRect(current_contour)
                m = cv2.moments(current_contour)

                c_x = int(m["m10"] / m["m00"])
                c_y = int(m["m01"] / m["m00"])

                if foreground[c_y, c_x].all() == np.array([0, 0, 0]).all():
                    continue

                x, y, w, h = rect

                if h < min_height or w < min_width:
                    continue

                rect_list.append(rect)
                hull = cv2.convexHull(current_contour)
                mask = self.create_pill_mask(hull)

                out = np.zeros_like(self.image)
                out[mask == 255] = self.image[mask == 255]

                out = cv2.copyMakeBorder(out, crop, crop, crop, crop, cv2.BORDER_CONSTANT, value=[0, 0, 0])

                if return_pill:
                    c_x = c_x + crop
                    c_y = c_y + crop
                    pill = out[c_y - int(crop / 2):c_y + int(crop / 2), c_x - int(crop / 2):c_x + int(crop / 2)]

                    if pill.size != 0:
                        yield [pill, current_contour]

                else:
                    yield [current_contour]

    elif method == 1:

        if thresholded is None:
            thresholded = self.extract_threshold(method=0)

        seperate_threshold = cv2.subtract(thresholded, edged)
        _, contours_thresh, _ = cv2.findContours(seperate_threshold, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)

        for current_contour in contours_thresh:

            rect = cv2.boundingRect(current_contour)
            x, y, w, h = rect

            m = cv2.moments(current_contour)

            c_x = int(m["m10"] / m["m00"])
            c_y = int(m["m01"] / m["m00"])

            if h < min_height or w < min_width:
                continue

            rect_list.append(rect)

            hull = cv2.convexHull(current_contour)
            mask = self.create_pill_mask(hull)

            out = np.zeros_like(self.image)
            out[mask == 255] = self.image[mask == 255]

            out = cv2.copyMakeBorder(out, crop, crop, crop, crop, cv2.BORDER_CONSTANT, value=[0, 0, 0])

            if return_pill:
                c_x = c_x + crop
                c_y = c_y + crop
                pill = out[c_y - int(crop / 2):c_y + int(crop / 2), c_x - int(crop / 2):c_x + int(crop / 2)]

                if pill.size != 0:
                    yield [pill, current_contour]

            else:
                yield [current_contour]






if __name__ == '__main__':
    while(True):
        ret, frame = cap.read()

        gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)

        cv2.imshow('frame',gray)
        if cv2.waitKey(1) & 0xFF == ord('q'):
            break

cap.release()
cv2.destroyAllWindows()
